﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BowArrow : Singleton<WRLDSBallPlugin>
{
    public Rigidbody rb;
    public Button scanButton;
    private float[] pitchData;

    private void Awake()
    {
        WRLDS.Init();
    }
    // Start is called before the first frame update
    void Start()
    {
        //WRLDS.OnMotionDataReceived += MotionHandler;
        rb = GetComponent<Rigidbody>();
        scanButton.onClick.AddListener(OnScan);

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void FixedUpdate()
    {
        if (pitchData != null)
        {
            //Debug.Log(pitchData[0].ToString());
            //rb.transform.eulerAngles = new Vector3(0, pitchData[0], 0);
        }

    }

    private void OnScan()
    {
        WRLDS.ScanForDevices();
    }

    void MotionHandler(string dataType, AndroidJavaObject motionDataObject)
    {
        AndroidJavaObject IMUPitchBufferObject;
        if (dataType == "PITCH_IMU")
        {
            IMUPitchBufferObject = motionDataObject.Get<AndroidJavaObject>("FloatData");
            pitchData = AndroidJNIHelper.ConvertFromJNIArray<float[]>(IMUPitchBufferObject.GetRawObject());
        }

    }
}
