﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using static WRLDSUtils; 

public class Ball : Singleton<WRLDSBallPlugin>
{
    public Rigidbody rb;
    public List<Rigidbody> balls;

    private float ballG = 0.0f;
    private List<float> ballForces = new List<float>();

    private BluetoothState state = 0;

    public Camera camera;
    private bool zoomOutCamera;

    /**
     * Bluetooth device address data store
     */
    private List<string> deviceAddressStore = new List<string>();

    private void Awake()
    {
        WRLDS.Init();
    }
    // Use this for initialization
    void Start()
    {
        // By calling the singelton instance of WRLDSAdapter, we instantiate
        // the plugin that communicate with the ball.

        // Let's scan for devices!
#if UNITY_IOS
        // For iOS we can scan directly after adding the state change handler.
        WRLDS.ScanForDevices();
        rb = GetComponent<Rigidbody>();

#elif UNITY_ANDROID
        // For Android, we scan inside the connection state handler so we know when
        // the SDK is ready for scanning for balls or if the connection is
        // dropped.

        WRLDS.StartConnectionStateEvents(ConnectionStateHandler);
        WRLDS.GetBounceEvents(true, BounceHandler);
        WRLDS.GetTiltEvents(true, TiltHandler);
#endif
    }

    // Update is called once per frame
    void Update ()
    {
#if UNITY_IOS
        if (ballG > 0.0f)
        {
            // The ball is going to move upwards in 10 units per second
            rb.velocity = new Vector3(0, ballG/6, 0);
            ballG = 0.0f;
        }
#elif UNITY_ANDROID
        for (int i = 0; i < deviceAddressStore.Count; i++)
        {
            if (ballForces[i] > 0.0f)
            {
                balls[i].velocity = new Vector3(0, ballForces[i] / 6, 0);
                ballForces[i] = 0.0f;
            }
        }
        if (zoomOutCamera)
        {
            camera.transform.Translate(0, 0, -10, Space.Self);
            zoomOutCamera = false;
        }
#endif
    }

#if UNITY_ANDROID
    void BounceHandler (BluetoothDevice device, int type, float sumG, int intensity) {
        if (deviceAddressStore.Count == 0)
            return; 

        for (int i = 0; i < deviceAddressStore.Count; i++)
        {
            if (device.getDeviceAddress() == deviceAddressStore[i])
                ballForces[i] = sumG;
        }
    }

    void TiltHandler (BluetoothDevice device, float roll, float pitch)
    {
        Debug.Log("Device Address: " + device.getDeviceAddress() + " Roll: " + roll + " Pitch: " + pitch);
    }
#endif

#if UNITY_IOS
    void FifoDataHandler(float[] fifoData)
    {
        var fifoString = "";
        for (int i = 0; i < fifoData.Length; i++) {
            fifoString += fifoData[i].ToString() + ", ";
        }
    }
#elif UNITY_ANDROID
    void ConnectionStateHandler(BluetoothDevice device, int connectionState) 
    {
        if (connectionState == -1) state = BluetoothState.LINK_LOSS;
        else if (connectionState == 0) state = BluetoothState.DISCONNECTED;
        else if (connectionState == 1)
        {
            state = BluetoothState.CONNECTED;

            if (CheckForNewDeviceAddress(device.getDeviceAddress()))
            {
                ballForces.Add(0.0f);
            }
            if (deviceAddressStore.Count == 2)
            {
                zoomOutCamera = true;
            }

        }
        else if (connectionState == 2) state = BluetoothState.CONNECTING;
        else if (connectionState == 3) state = BluetoothState.DISCONNECTING;

        Debug.Log("ConnectionState: " + state);
    }

    private void OnDestroy()
    {
        WRLDS.StopConnectionStateEvents(ConnectionStateHandler);
    }


    bool CheckForNewDeviceAddress(string newDeviceAddress)
    {
        //Check from which sensor the data is coming from
        if (deviceAddressStore.Count == 0)
        {
            deviceAddressStore.Add(newDeviceAddress);
            return true;
        }

        else
        {
            //We set the default value to true because we can only iterate the loop one-by-one value
            bool foundNewDevice = true;

            foreach (string deviceAddress in deviceAddressStore.ToList())
            {
                if (deviceAddress.Equals(newDeviceAddress))
                {
                    foundNewDevice = false;
                }

            }
            if (foundNewDevice)
            {
                deviceAddressStore.Add(newDeviceAddress);
                return true;
            }
            else
                return false;
        }
    }
#endif
}
