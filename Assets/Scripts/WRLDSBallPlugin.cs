﻿using UnityEngine;
using System;
using System.Collections;
using System.Runtime.InteropServices;
using AOT;
using System.Collections.Generic;
using System.Linq;
using static WRLDSUtils;

/**
 * This class interfaces with the Unity WRLDS iOS Native plugin
 */
public sealed class WRLDSBallPlugin : WRLDSBasePlugin
{
#if UNITY_IOS

    #region Native Implementation Interface
    [DllImport("__Internal")]
    private static extern void initializer();

    [DllImport("__Internal")]
    private static extern void finalizer();

    // Interaction Delegate Callback Setters
    [DllImport("__Internal")]
    private static extern void set_interaction_delegate_ball_did_bounce_callback(BallDidBounceCallback callback);

    [DllImport("__Internal")]
    private static extern void set_interaction_delegate_ball_did_shake_callback(BallDidShakeCallback callback);

    [DllImport("__Internal")]
    private static extern void set_interaction_delegate_ball_did_receive_fifo_data_callback(BallDidReceiveFifoDataCallback callback);

    [DllImport("__Internal")]
    private static extern void set_connection_delegate_ball_did_change_state_callback(BallDidChangeConnectionStateCallback callback);

    // Properties
    [DllImport("__Internal")]
    private static extern void set_low_threshold(double threshold); // Default 30.0

    [DllImport("__Internal")]
    private static extern void set_high_threshold(double threshold); // Default 60.0

    [DllImport("__Internal")]
    private static extern void set_shake_interval(double intervalSeconds); // Default 0.250

    [DllImport("__Internal")]
    private static extern string get_connection_state();

    [DllImport("__Internal")]
    private static extern string get_device_address();

    // Actions
    [DllImport("__Internal")]
    private static extern void start_scanning();

    [DllImport("__Internal")]
    private static extern void stop_scanning();

    [DllImport("__Internal")]
    private static extern void connect_to_ball(string address);

    #endregion


    #region Native Callbacks
    // Define function pointers that can be passed as arguments
    private delegate void BallDidBounceCallback(int type, float sumG);
    private delegate void BallDidShakeCallback(double shakeProgress);
    private delegate void BallDidReceiveFifoDataCallback(IntPtr fifoData, int length);
    private delegate void BallDidChangeConnectionStateCallback(int state, string stateMessage);
    #endregion


    #region Native Callback function pointer retainers
    // Holds on to the function pointers so that they cannot be deallocated when GC runs
    private BallDidBounceCallback _ball_did_bounce_callback_holder;
    private BallDidShakeCallback _ball_did_shake_callback_holder;
    private BallDidReceiveFifoDataCallback _ball_did_receive_fifo_data_callback_holder;
    private BallDidChangeConnectionStateCallback _ball_did_change_connection_state_callback_holder;
    #endregion

    public delegate void BounceDelegate(int bounceType, float sumG);
    public delegate void ShakeDelegate(float shakeProgress);
    public delegate void FifoDataDelegate(float[] fifoDataObject);
    public delegate void ConnectionStateChangeDelegate(int connectionState, string stateMessage);

    public event BounceDelegate OnBounce;
    public event ShakeDelegate OnShake;
    public event FifoDataDelegate OnFifoData;
    public event ConnectionStateChangeDelegate OnConnectionStateChange;
#elif UNITY_ANDROID

    private AndroidBounceCallback onBounceCallback;
    private AndroidTiltCallback onTiltCallback;

#endif
    void Awake()
    {
#if UNITY_IOS
        DontDestroyOnLoad(this.gameObject);

        // Store the callback functions
        _ball_did_bounce_callback_holder = new BallDidBounceCallback(_ball_did_bounce_callback);
        _ball_did_shake_callback_holder = new BallDidShakeCallback(_ball_did_shake_callback);
        _ball_did_receive_fifo_data_callback_holder = new BallDidReceiveFifoDataCallback(_ball_did_receive_fifo_data_callback);
        _ball_did_change_connection_state_callback_holder = new BallDidChangeConnectionStateCallback(_ball_did_change_connection_state_callback);

        // Set native pointers to internal callbacks
        set_interaction_delegate_ball_did_bounce_callback(_ball_did_bounce_callback_holder);
        set_interaction_delegate_ball_did_shake_callback(_ball_did_shake_callback_holder);
        set_interaction_delegate_ball_did_receive_fifo_data_callback(_ball_did_receive_fifo_data_callback_holder);
        set_connection_delegate_ball_did_change_state_callback(_ball_did_change_connection_state_callback_holder);

        initializer();
#elif UNITY_ANDROID
        DontDestroyOnLoad(this.gameObject);

        SDK = new AndroidJavaObject("com.wrlds.api.BallProvider", new object[] { GetAndroidUnityPlayerActivity() });
        bleDeviceProvider = SDK.Get<AndroidJavaObject>("bleDeviceProvider");
        userProvider = SDK.Get<AndroidJavaObject>("userAccountManager");

        Debug.Log($"[SDK initialized -> product type: Ball");
#endif
    }

    void OnApplicationPause(bool pause)
    {
#if UNITY_ANDROID
        if (!pause)
        {
            if (SDK != null)
                SDK.Call("onStart");
        }
        if (pause)
        {
            if (SDK != null)
                SDK.Call("onStop");
        }
#endif
    }

    void OnDestroy()
    {
#if UNITY_ANDROID
        SDK.Call("onDestroy");
#endif
    }

#if UNITY_IOS
    #region Internal Interaction Callbacks
    [MonoPInvokeCallback(typeof(BallDidBounceCallback))]
    private void _ball_did_bounce_callback(int type, float sumG)
    {
        Debug.Log("WRLDSBall_iOS::BallDidBounce(" + type + ", " + sumG + ")");
        if (OnBounce != null)
        {
            OnBounce.Invoke(type, sumG);
        }
    }

    [MonoPInvokeCallback(typeof(BallDidShakeCallback))]
    private static void _ball_did_shake_callback(double shakeProgress)
    {
        // TODO: Trigger Unity based callback
        Debug.Log("WRLDSBall_iOS::BallDidShake(" + shakeProgress + ")");
    }

    [MonoPInvokeCallback(typeof(BallDidReceiveFifoDataCallback))]
    private void _ball_did_receive_fifo_data_callback(IntPtr fifoData, int length)
    {
        var floatArray = new float[length];
        Marshal.Copy(fifoData, floatArray, 0, length);

        if (OnFifoData != null)
        {
            OnFifoData.Invoke(floatArray);
        }
    }

    [MonoPInvokeCallback(typeof(BallDidChangeConnectionStateCallback))]
    private void _ball_did_change_connection_state_callback(int state, string stateMessage)
    {
        Debug.Log("WRLDSBall_iOS::BallDidChangeConnectionState(" + state + ")");
        if (OnConnectionStateChange != null)
        {
            OnConnectionStateChange.Invoke(state, stateMessage);
        }
    }
    #endregion
#endif

    private void _teardown()
    {
#if UNITY_IOS
        set_interaction_delegate_ball_did_bounce_callback(null);
        set_interaction_delegate_ball_did_shake_callback(null);
        set_connection_delegate_ball_did_change_state_callback(null);

        _ball_did_bounce_callback_holder = null;
        _ball_did_shake_callback_holder = null;
#endif
    }

#if UNITY_ANDROID
    public void GetBounceEvents(bool enable, AndroidBounceCallback.BounceDelegate dataCallback)
    {
        if (onBounceCallback == null)
            onBounceCallback = new AndroidBounceCallback();

        if (enable)
            onBounceCallback.AddSubscriber(dataCallback);   //Add the Callback supplied in the parameter to the Callback pub/sub list 
        else { onBounceCallback.RemoveSubscriber(dataCallback); }
    }

#endif

#if UNITY_ANDROID
    public void GetTiltEvents(bool enable, AndroidTiltCallback.TiltDelegate dataCallback)
    {
        if (onTiltCallback == null)
            onTiltCallback = new AndroidTiltCallback();

        if (enable)
            onTiltCallback.AddSubscriber(dataCallback);   //Add the Callback supplied in the parameter to the Callback pub/sub list 
        else { onTiltCallback.RemoveSubscriber(dataCallback); }
    }

#endif

    public override void ScanForDevices()
    {
#if UNITY_IOS
        start_scanning();
#elif UNITY_ANDROID
        bleDeviceProvider.Call("scanForDevicesWithList", new object[] { GetAndroidUnityPlayerActivity() });
#endif
    }

    public override string GetDeviceAddress()
    {
#if UNITY_IOS
        string hash = get_device_address();
#elif UNITY_ANDROID
        string hash = SDK.Call<string>("getDeviceAddress");
#else
        string hash = "00:00:00:00:00:00";
#endif
        return hash;
    }

    public override void ConnectDevice(string hash)
    {
#if UNITY_IOS
        Debug.Log("Connecting to device: " + hash);

        connect_to_ball(hash);
#elif UNITY_ANDROID
        SDK.Call("connectDevice", hash);
#endif
    }


#if UNITY_ANDROID
    /**
     * Function for retrieving the total bounce count that a user has accumulated across all WRLDS games
     */
    public int GetTotalBounceCount()
    {
        string bounceCount = SDK.Call<string>("getUserData", "bounceCount");

        if (int.TryParse(bounceCount, out int parseResult))
            return parseResult;
        else
        {
            return -1;
        }
    }
#endif

    public void FetchBounceLeaderboard()
    {
#if UNITY_ANDROID
        SDK.Call("fetchLeaderboard");
#endif
    }

#if UNITY_ANDROID
    public BounceLeaderboard GetLeaderboard()
    {
        BounceLeaderboard leaderboard = GetLeaderBoard(SDK.Call<string>("getLeaderboard"));
        return leaderboard;
    }
#endif

#if UNITY_ANDROID

    public class AndroidBounceCallback : AndroidJavaProxy
    {
        public delegate void BounceDelegate(BluetoothDevice device, int bounceType, float sumG, int intensity);
        public event BounceDelegate OnBounceReceived;

        public void AddSubscriber(BounceDelegate callback) { OnBounceReceived += callback; }
        public void RemoveSubscriber(BounceDelegate callback) { OnBounceReceived -= callback; }

        public AndroidBounceCallback() : base("com.wrlds.api.listeners.ball.BounceListener")
        {
            WRLDS.SDK.Call("setBounceListener", this);
        }

        /**
         * Detects a tilt event.
         * @param device: The device where the data originates from
         * @param roll: The roll angle that the scooter is in (degrees)
         * @param pitch: The pitch angle that the scooter is in (degrees)
         * @param intensity: The intensity of the high G event
         */
        void onBounceReceived(AndroidJavaObject device, int bounceType, float sumG, int intensity)
        {
            if (OnBounceReceived != null)
            {
                OnBounceReceived.Invoke(WRLDSUtils.GetJavaBluetoothClass(device), bounceType, sumG, intensity);
                device.Dispose();
            }
        }

    }

    public class AndroidTiltCallback : AndroidJavaProxy
    {
        public delegate void TiltDelegate(BluetoothDevice device, float roll, float pitch);
        public event TiltDelegate OnTiltReceived;

        public void AddSubscriber(TiltDelegate callback) { OnTiltReceived += callback; }
        public void RemoveSubscriber(TiltDelegate callback) { OnTiltReceived -= callback; }

        public AndroidTiltCallback() : base("com.wrlds.api.listeners.TiltListener")
        {
            WRLDS.SDK.Call("setTiltListener", this);
        }

        /**
         * Detects a tilt event.
         * @param roll: The roll angle that the scooter is in (degrees)
         * @param pitch: The pitch angle that the scooter is in (degrees)
         */
        void onTiltReceived(AndroidJavaObject device, float roll, float pitch)
        {
            if (OnTiltReceived != null)
            { OnTiltReceived.Invoke(WRLDSUtils.GetJavaBluetoothClass(device), roll, pitch); }
        }
    }
#endif

}






