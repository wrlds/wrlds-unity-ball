﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static WRLDSUtils;

public class RollingBall : Singleton<WRLDSBallPlugin>
{
    public Rigidbody rb;
    public float speed;
    private float[] positionData;
    private float[] quaternionData;
    public Button resetButton, scanButton;

    // Start is called before the first frame update
    void Start()
    {
#if UNITY_ANDROID
        resetButton.onClick.AddListener(OnReset);
        scanButton.onClick.AddListener(OnScan);
        WRLDS.GetBounceEvents(true, BounceHandler);
        //WRLDS.OnMotionDataReceived += MotionHandler;
        rb = GetComponent<Rigidbody>();
#endif

    }

    private void OnReset()
    {
        Vector3 reset = new Vector3(0, 2, 0);
        rb.transform.position = (reset);
        //rb.transform.rotation = Quaternion.Euler(0f, 0f, 0);
    }

    private void OnScan()
    {
        WRLDS.ScanForDevices();
    }

    private void FixedUpdate()
    {
        if (quaternionData != null)
        {
            Quaternion ballRotation = new Quaternion(-quaternionData[1], quaternionData[2], quaternionData[0], -quaternionData[3]);
            //Quaternion ballRotation = new Quaternion(quaternionData[0], quaternionData[1], quaternionData[2], quaternionData[3]);

            rb.transform.rotation = ballRotation;
        }
        if (positionData != null)  {
            float moveHorizontal = positionData[0]* 3000;  //Input.GetAxis("Horizontal");
            float moveVertical = positionData[1] * 3000; //Input.GetAxis("Vertical");

            //Need to translate ball movement into graphics simulation
            //Simplify 9-DOF data to 2-DOF (horizontal/vertical)

            Vector3 movement = new Vector3(moveVertical, 0.0f, -moveHorizontal);
            //rb.AddForce(movement * speed);
            rb.MovePosition(transform.position + movement);
        }
    }

#if UNITY_ANDROID
    void BounceHandler(BluetoothDevice device, int type, float sumG, int intensity)
    {
        
    }
#endif

    void MotionHandler(AndroidJavaObject positionDataObject, AndroidJavaObject quaternionDataObject)
    {
        AndroidJavaObject positionBufferObject = positionDataObject.Get<AndroidJavaObject>("FloatData");
        AndroidJavaObject quaternionBufferObject = quaternionDataObject.Get<AndroidJavaObject>("FloatData");

        positionData = AndroidJNIHelper.ConvertFromJNIArray<float[]>(positionBufferObject.GetRawObject());
        quaternionData = AndroidJNIHelper.ConvertFromJNIArray<float[]>(quaternionBufferObject.GetRawObject());
    }
}
