﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static WRLDSUtils;

public class ButtonState : MonoBehaviour
{
    private BluetoothState state = 0;
    public Text scanText;
    private int alphaCount = 100;
    private int connectingCount = 0;
    private bool makeTransparent = true;
    private bool increment = false;
    Color buttonColor;
    float timer = 0.0f;

    WRLDSBasePlugin WRLDS;

    // Start is called before the first frame update
    void Start()
    {
#if UNITY_ANDROID        
        WRLDS = WRLDSBasePlugin.GetSDK();
        WRLDS.StartConnectionStateEvents(ConnectionStateHandler);
#endif

        this.GetComponent<Button>().onClick.AddListener(OnScan);
        Color buttonColor = this.GetComponent<Image>().color;
        Text scanText = this.GetComponent<Text>();
    }

#if UNITY_ANDROID
    private void ConnectionStateHandler(BluetoothDevice device, int connectionState) 
    {
        if (connectionState == -1) state = BluetoothState.LINK_LOSS;
        else if (connectionState == 0) state = BluetoothState.DISCONNECTED;
        else if (connectionState == 1) state = BluetoothState.CONNECTED;
        else if (connectionState == 2) state = BluetoothState.CONNECTING;
        else if (connectionState == 3) state = BluetoothState.DISCONNECTING;

        Debug.Log("ConnectionState ButtonState: " + state);
    }
#endif

    // Update is called once per frame
    void Update()
    {
        if (state == BluetoothState.CONNECTED)
        {
            this.GetComponent<Image>().color = Color.grey;
            this.GetComponent<Image>().fillCenter = true;
            scanText.color = new Color(20,20,20);
            scanText.text = "connected";
        }
        else if (state == BluetoothState.CONNECTING)
        {
            this.GetComponent<Image>().fillCenter = false;
            timer += Time.deltaTime;
            int seconds = Convert.ToInt32(timer % 60);

            if (seconds % 2 == 0) scanText.text = "connecting...";
            else { scanText.text = "connecting.."; }
            scanText.color = Color.grey;

            if (alphaCount <= 0) increment = true;
            else if (alphaCount >= 100) increment = false;

            if (increment)
            {
                alphaCount = alphaCount + 3;
                this.GetComponent<Image>().color = new Color(buttonColor.r, buttonColor.g, buttonColor.b, alphaCount / 100f);
            }
            if (!increment)
            {
                alphaCount = alphaCount - 3;
                this.GetComponent<Image>().color = new Color(buttonColor.r, buttonColor.g, buttonColor.b, alphaCount / 100f);
            }
        }
        else if (state == BluetoothState.DISCONNECTED)
        {
            this.GetComponent<Image>().color = Color.grey;
            this.GetComponent<Image>().fillCenter = false;
            scanText.color = Color.grey;
            scanText.text = "disconnected..";
        }
        else
        {
            this.GetComponent<Image>().fillCenter = false;
            scanText.color = Color.grey;
            scanText.text = "find my device";
        }
    }

    private void OnScan()
    {
        WRLDS.ScanForDevices();
    }

    private void OnDestroy()
    {
#if UNITY_ANDROID
        WRLDS.StopConnectionStateEvents(ConnectionStateHandler);
#endif
    }

}
