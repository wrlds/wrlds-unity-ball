﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static WRLDSUtils;

/**
 * The WRLDSBasePlugin is a base class that provides the functionality that all WRLDS related products have in common 
 * This includes features such as BLE scanning, User logins are the same for most products as well as boilerplate callbacks 
 * 
 * Classes that inherit from WRLDSBasePlugin can override methods if the implementation deviates from the default scenario
 */
public abstract class WRLDSBasePlugin : Singleton<WRLDSBasePlugin>
{
#if UNITY_ANDROID
    public AndroidJavaObject SDK;
    protected AndroidJavaObject bleDeviceProvider;
    protected AndroidJavaObject userProvider;

    protected AndroidRawAccelerationDataCallback onRawAccelerationDataCallback;
    protected AndroidQuaternionDataCallback onQuaternionDataCallback;
    protected AndroidBLEConnectionStateCallback onConnectionStateCallback;
    protected AndroidGenericCallback onGenericCallback;
    protected UserActionCallbacks userActionCallbacks;
    protected UserDataCallbacks userDataCallbacks;

#endif


    #region Methods


#if UNITY_ANDROID
    /**
     * Returns a reference to the SDK from scripts that do not inherit the SDK object
     * 
     */
    public static WRLDSBasePlugin GetSDK()
    {
        return (WRLDSBasePlugin)FindObjectOfType(typeof(Singleton));
    }
    /*         
     * First the SDK needs the activity context, so we create a reference to the UnityPlayer class.
     * Second we get a reference to the activity.
     */
    protected static AndroidJavaObject GetAndroidUnityPlayerActivity()
    {

        AndroidJavaClass unityActivity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject activity = unityActivity.GetStatic<AndroidJavaObject>("currentActivity");

        if (activity == null)
        {
            Debug.Log($"[Unable to get: {activity}");
        }
        return activity;
    }
#endif

    public virtual void ScanForDevices()
    {
#if UNITY_ANDROID
        bleDeviceProvider.Call("scanForDevicesWithList", new object[] { GetAndroidUnityPlayerActivity() });
#endif
    }

    public virtual void ScanForDevices(int scanDuration)
    {
#if UNITY_ANDROID
        bleDeviceProvider.Call("scanForDevicesWithList", new object[] { GetAndroidUnityPlayerActivity() }, scanDuration);
#endif
    }

    public virtual int GetConnectionState()
    {
#if UNITY_ANDROID
        int connectionState = SDK.Call<int>("getConnectionState");

#else
        int connectionState = -1;
#endif
        return connectionState;
    }

#if UNITY_ANDROID
    public virtual void StartConnectionStateEvents(AndroidBLEConnectionStateCallback.ConnectionStateChangedDelegate callback)
    {
        if (onConnectionStateCallback == null)
            onConnectionStateCallback = new AndroidBLEConnectionStateCallback();

        onConnectionStateCallback.AddSubscriber(callback);
    }

    public virtual void StopConnectionStateEvents(AndroidBLEConnectionStateCallback.ConnectionStateChangedDelegate callback)
    {
        if (onConnectionStateCallback != null)
            onConnectionStateCallback.RemoveSubscriber(callback);
    }
#endif

#if UNITY_ANDROID
    public virtual void StartAccelerationDataStream(AndroidRawAccelerationDataCallback.RawAccelerationDataDelegate dataCallback)
    {
        if (onRawAccelerationDataCallback == null)
            onRawAccelerationDataCallback = new AndroidRawAccelerationDataCallback();

        if (onRawAccelerationDataCallback.GetSubscriberCount() == 0)
            onRawAccelerationDataCallback.SetListener();

        onRawAccelerationDataCallback.AddSubscriber(dataCallback);   //Add the Callback supplied in the parameter to the Callback pub/sub list 
    }
#endif

#if UNITY_ANDROID
    public void GetGenericCallbacks(bool enable, AndroidGenericCallback.GenericDelegate dataCallback)
    {
        if (onGenericCallback == null)
            onGenericCallback = new AndroidGenericCallback();

        if (enable)
            onGenericCallback.AddSubscriber(dataCallback);   //Add the Callback supplied in the parameter to the Callback pub/sub list 
        else { onGenericCallback.RemoveSubscriber(dataCallback); }
    }
#endif

#if UNITY_ANDROID
    public virtual void StopAccelerationDataStream(AndroidRawAccelerationDataCallback.RawAccelerationDataDelegate dataCallback)
    {
        if (onRawAccelerationDataCallback != null)
        {
            onRawAccelerationDataCallback.RemoveSubscriber(dataCallback);

            if (onRawAccelerationDataCallback.GetSubscriberCount() == 0)
            {
                onRawAccelerationDataCallback.RemoveListener();
            }
        }
    }
#endif

#if UNITY_ANDROID
    public virtual void StartQuaternionDataStream(AndroidQuaternionDataCallback.QuaternionDataDelegate dataCallback)
    {
        if (onQuaternionDataCallback == null)
            onQuaternionDataCallback = new AndroidQuaternionDataCallback();

        if (onQuaternionDataCallback.GetSubscriberCount() == 0)
            onQuaternionDataCallback.SetListener();
        
        onQuaternionDataCallback.AddSubscriber(dataCallback);   //Add the Callback supplied in the parameter to the Callback pub/sub list 
    }

    public virtual void StopQuaternionDataStream(AndroidQuaternionDataCallback.QuaternionDataDelegate dataCallback)
    {
        if (onQuaternionDataCallback != null)
        {
            onQuaternionDataCallback.RemoveSubscriber(dataCallback);

            if (onQuaternionDataCallback.GetSubscriberCount() == 0)
            {
                onQuaternionDataCallback.RemoveListener();
            }
        }
    }

#endif

    public virtual void SetBLESettings(WRLDSUtils.BLESettings settings)
    {
#if UNITY_ANDROID
        Dictionary<string, string> dictionary = new Dictionary<string, string>();
        dictionary.Add("autoConnect", settings.AutoConnect.ToString());
        dictionary.Add("bleConnectionInterval", settings.ConnectionInterval.ToString());
        dictionary.Add("keepBLEConnectionInBackground", settings.BleConnectionInBackground.ToString());

        Debug.Log("bleConnectionInterval: " + settings.ConnectionInterval.ToString());

        bleDeviceProvider.Call<bool>("setBleSettings", WRLDSUtils.ConvertDictionaryToJavaMap(dictionary));
#else
        int connectionState = -1;
#endif

    }
    
    public virtual string GetDeviceAddress()
    {
#if UNITY_ANDROID
        string hash = SDK.Call<string>("getDeviceAddress");
#else
        string hash = "00:00:00:00:00:00";
#endif
        return hash;
    }

    public virtual void DisconnectDevice()
    {
#if UNITY_IOS
//
#elif UNITY_ANDROID
        bleDeviceProvider.Call("disconnectDevice");
#endif
    }

    public virtual void ConnectDevice(string hash)
    {
#if UNITY_ANDROID
        bleDeviceProvider.Call("connectDevice", hash);
#endif
    }

#if UNITY_ANDROID
    public virtual void SignUp(string mail, string password, UserActionCallbacks.SignUpDelegate callback)
    {
        if (userActionCallbacks == null)
            userActionCallbacks = new UserActionCallbacks();

        userActionCallbacks.SignUpRequests.Add(callback);
        userActionCallbacks.OnSignUp += callback;

        userProvider.Call("signUp", new object[] { mail, password });
    }
#endif

#if UNITY_ANDROID
    public virtual void SignIn(string mail, string password, UserActionCallbacks.SignInDelegate callback)
    {
        if (userActionCallbacks == null)
            userActionCallbacks = new UserActionCallbacks();

        userActionCallbacks.SignInRequests.Add(callback);
        userActionCallbacks.OnSignIn += callback;

        userProvider.Call("signIn", mail, password);
    }
#endif

#if UNITY_ANDROID
    public virtual void SignOut(UserActionCallbacks.SignOutDelegate callback)
    {
        if (userActionCallbacks == null)
            userActionCallbacks = new UserActionCallbacks();

        userActionCallbacks.SignOutRequests.Add(callback);
        userActionCallbacks.OnSignOut += callback;

        userProvider.Call("signOut");
    }
#endif

#if UNITY_ANDROID
    public virtual void ResendConfirmationLink(string mail, UserActionCallbacks.ResendConfirmationLinkDelegate callback)
    {
        if (userActionCallbacks == null)
            userActionCallbacks = new UserActionCallbacks();

        userActionCallbacks.ResendConfirmationLinkRequests.Add(callback);
        userActionCallbacks.OnResendConfirmationLink += callback;

        userProvider.Call("resendConfirmationLink", mail);
    }
#endif

#if UNITY_ANDROID
    public virtual bool CheckLoginState()
    {
        return CheckLoginState(null);
    }
#endif

#if UNITY_ANDROID
    public virtual bool CheckLoginState(string user)
    {
        return userProvider.Call<bool>("checkLoginState", user);
    }
#endif

#if UNITY_ANDROID
    public virtual string GetCurrentUser()
    {
        return userProvider.Call<string>("getCurrentUser");

    }
#endif

#if UNITY_ANDROID
    public virtual void ChangePassword(string oldPassword, string newPassword, UserActionCallbacks.PasswordChangeDelegate callback)
    {
        if (userActionCallbacks == null)
            userActionCallbacks = new UserActionCallbacks();

        userActionCallbacks.PasswordChangeRequests.Add(callback);
        userActionCallbacks.OnPasswordChange += callback;

        userProvider.Call("changePassword", oldPassword, newPassword);
    }
#endif

#if UNITY_ANDROID
    public virtual void RequestNewPassword(string mail, UserActionCallbacks.RequestNewPasswordDelegate callback)
    {
        if (userActionCallbacks == null)
            userActionCallbacks = new UserActionCallbacks();

        userActionCallbacks.NewPasswordRequests.Add(callback);
        userActionCallbacks.OnNewPasswordRequest += callback;

        userProvider.Call("requestNewPassword", mail);
    }
#endif

#if UNITY_ANDROID
    public virtual void ResetPassword(string newPassword, string code, UserActionCallbacks.PasswordResetDelegate callback)
    {
        if (userActionCallbacks == null)
            userActionCallbacks = new UserActionCallbacks();

        userActionCallbacks.PasswordResetRequests.Add(callback);
        userActionCallbacks.OnPasswordReset += callback;

        userProvider.Call("resetPassword", newPassword, code);
    }
#endif

#if UNITY_ANDROID
    public virtual void SubmitMailCode(string attributeName, string code, UserActionCallbacks.AttributeConfirmDelegate callback)
    {

        if (userActionCallbacks == null)
            userActionCallbacks = new UserActionCallbacks();

        userActionCallbacks.AttributeConfirmRequests.Add(callback);
        userActionCallbacks.OnAttributeConfirmed += callback;

        userProvider.Call("confirmAttribute", attributeName, code);
    }
#endif


#if UNITY_ANDROID
    public virtual void ChangeAttribute(string attributeKey, string attributeValue, UserActionCallbacks.AttributeChangedDelegate callback)
    {

        if (userActionCallbacks == null)
            userActionCallbacks = new UserActionCallbacks();

        userActionCallbacks.AttributeChangeRequests.Add(callback);
        userActionCallbacks.OnAttributeChanged += callback;

        userProvider.Call("changeAttribute", attributeKey, attributeValue);
        
    }
#endif

#if UNITY_ANDROID
    public virtual void DeleteUserAccount(string user, UserActionCallbacks.DeleteUserAccountDelegate callback)
    {
        if (userActionCallbacks == null)
            userActionCallbacks = new UserActionCallbacks();

        userActionCallbacks.DeleteUserAccountRequests.Add(callback);
        userActionCallbacks.OnDeleteUserAccount += callback;

        userProvider.Call("deleteUserAccount", user);
    }
#endif

    public virtual string[] GetUserData(string[] dataToRetrieve)
    {
        if (dataToRetrieve == null)
            return null;
#if UNITY_ANDROID
        string[] data = SDK.Call<string[]>("getUserData", WRLDSUtils.ToJavaStringArray(dataToRetrieve));
#else
        string[] data = null;
#endif
        return data;
    }

#if UNITY_ANDROID
    public virtual void PostUserDataToDatabase(Dictionary<string, string> userData, UserDataCallbacks.UserDataRetrievedDelegate callback)
    {
        if (userDataCallbacks == null)
            userDataCallbacks = new UserDataCallbacks();

        userDataCallbacks.UserDataRequests.Add(callback);
        userDataCallbacks.OnUserDataRetrieved += callback;

        userProvider.Call("postUserData", WRLDSUtils.ConvertDictionaryToJavaMap(userData));
    }
#endif

    #endregion

#if UNITY_ANDROID

    /**
     *  <summary>
     *  These Android callbacks implement Java listeners in order to send data from the SDK -> Unity
     *  <code>AndroidRawAccelerationDataCallback</code> A C# callback for events that a java listener receives
     *  <code>MotionDataDelegate</code> The delegate that gets called when a new event is received
     *  <code>ConvertObjectToFloatArray</code> The data needs to be sent in an encapsulated class for communication between SDK -> Unity
     *  </summary>      
     */
    public class AndroidRawAccelerationDataCallback : AndroidJavaProxy
    {
        public delegate void RawAccelerationDataDelegate(BluetoothDevice device, float[] motionData);
        public event RawAccelerationDataDelegate OnRawAccelerationDataReceived;

        public void AddSubscriber(RawAccelerationDataDelegate callback) { OnRawAccelerationDataReceived += callback; }
        public void RemoveSubscriber(RawAccelerationDataDelegate callback) { OnRawAccelerationDataReceived -= callback; }

        public int GetSubscriberCount()
        {
            if (OnRawAccelerationDataReceived == null) return 0;
            else { return OnRawAccelerationDataReceived.GetInvocationList().Length; }
        }

        public AndroidRawAccelerationDataCallback() : base("com.wrlds.api.listeners.RawAccelerationDataListener")
        {
            WRLDS.SDK.Call("setRawAccelerationDataListener", this);    //Set the needed Callbacks that get data from Android Listeners  
        }

        public void SetListener()
        {
            WRLDS.SDK.Call("setRawAccelerationDataListener", this);
        }
        public void RemoveListener()
        {
            WRLDS.SDK.Call("removeRawAccelerationDataListener"); //Remove the listener
        }

        // This callback method is called when the Java listener receives data
        void onRawAccelerationDataReceived(AndroidJavaObject device, AndroidJavaObject motionDataObject)
        {
            ///<code>ConvertObjectToFloatArray</code> is needed to convert the data from SDK -> Unity
            if (OnRawAccelerationDataReceived != null)
                OnRawAccelerationDataReceived.Invoke(WRLDSUtils.GetJavaBluetoothClass(device), WRLDSUtils.ConvertObjectToFloatArray(motionDataObject));

            motionDataObject.Dispose();
            device.Dispose();
        }
    }

    public class AndroidQuaternionDataCallback : AndroidJavaProxy
    {

        public delegate void QuaternionDataDelegate(BluetoothDevice device, float[] quaternionData);
        public event QuaternionDataDelegate OnQuaternionDataReceived;

        public void AddSubscriber(QuaternionDataDelegate callback) { OnQuaternionDataReceived += callback; }
        public void RemoveSubscriber(QuaternionDataDelegate callback) { OnQuaternionDataReceived -= callback; }
        public int GetSubscriberCount()
        {
            if (OnQuaternionDataReceived == null) return 0;
            else { return OnQuaternionDataReceived.GetInvocationList().Length; }
        }

        public AndroidQuaternionDataCallback() : base("com.wrlds.api.listeners.QuaternionDataListener")
        {
            SetListener();
        }
        public void SetListener()
        {
            WRLDS.SDK.Call("setQuaternionDataListener", this);
        }
        public void RemoveListener()
        {
            WRLDS.SDK.Call("removeQuaternionDataListener"); //Remove the listener
        }


        // This callback method is called when the Java listener receives data
        void onQuaternionDataReceived(AndroidJavaObject device, AndroidJavaObject quaternionDataObject)
        {
            ///<code>ConvertObjectToFloatArray</code> is needed to convert the data from SDK -> Unity
            if (OnQuaternionDataReceived != null)
                OnQuaternionDataReceived.Invoke(WRLDSUtils.GetJavaBluetoothClass(device), WRLDSUtils.ConvertObjectToFloatArray(quaternionDataObject));

            quaternionDataObject.Dispose();
            device.Dispose();
        }
    }

    public class AndroidBLEConnectionStateCallback : AndroidJavaProxy
    {
        public delegate void ConnectionStateChangedDelegate(BluetoothDevice device, int connectionState);
        public event ConnectionStateChangedDelegate OnConnectionStateChanged;

        public void AddSubscriber(ConnectionStateChangedDelegate callback) { OnConnectionStateChanged += callback; }
        public void RemoveSubscriber(ConnectionStateChangedDelegate callback) { OnConnectionStateChanged -= callback; }
        public int GetSubscriberCount()
        {
            if (OnConnectionStateChanged == null) return 0;
            else { return OnConnectionStateChanged.GetInvocationList().Length; }
        }

        public AndroidBLEConnectionStateCallback() : base("com.wrlds.api.listeners.ConnectionStateListener")
        {
            WRLDS.bleDeviceProvider.Call("setConnectionStateListener", this);
        }

        void onConnectionStateChanged(AndroidJavaObject device, int connectionState)
        {
            if (OnConnectionStateChanged != null)
                OnConnectionStateChanged.Invoke(WRLDSUtils.GetJavaBluetoothClass(device), connectionState);

            device.Dispose();
        }
    }

    public class AndroidGenericCallback : AndroidJavaProxy
    {
        public delegate void GenericDelegate(string action, bool state, string message, AndroidJavaObject result);
        public event GenericDelegate OnGenericReceived;

        public void AddSubscriber(GenericDelegate callback) { OnGenericReceived += callback; }
        public void RemoveSubscriber(GenericDelegate callback) { OnGenericReceived -= callback; }

        public AndroidGenericCallback() : base("com.wrlds.api.listeners.ball.GenericListener")
        {
            WRLDS.userProvider.Call("setGenericListener", this);
        }
        void onGenericReceived(string action, bool state, string message, AndroidJavaObject result)
        {
            if (OnGenericReceived != null)
            {
                OnGenericReceived.Invoke(action, state, message, result);
                result.Dispose();
            }
        }
    }

    public class UserDataCallbacks : AndroidJavaProxy
    {
        public delegate void UserDataRetrievedDelegate(bool state, string jsonData);
        public event UserDataRetrievedDelegate OnUserDataRetrieved;
        public List<UserDataRetrievedDelegate> UserDataRequests = new List<UserDataRetrievedDelegate>();

        public UserDataCallbacks() : base("com.wrlds.api.listeners.UserDataListener")
        {
            WRLDS.userProvider.Call("setUserDataListener", this);
        }
        void onUserAction(bool state, string jsonData)
        {
            if (OnUserDataRetrieved != null)
                OnUserDataRetrieved.Invoke(state, jsonData);

            if (UserDataRequests.Count > 0)
            {
                OnUserDataRetrieved -= UserDataRequests[0];
                UserDataRequests.RemoveAt(0);
            }
        }
    }

    /**
     * We use the callbacks for user account related actions differently than the other callbacks
     * All account actions are grouped in a single class to prevent creating many AndroidJavaProxy classes
     * These account actions do not require listening but just one callback per call. 
     * Therefore we unsubscribe from the delegate when the callback is received, so that delegates do not listen to other callbacks
     */
    public class UserActionCallbacks : AndroidJavaProxy
    {
        public delegate void SignUpDelegate(bool state, string message);
        public event SignUpDelegate OnSignUp;
        public List<SignUpDelegate> SignUpRequests = new List<SignUpDelegate>();

        public delegate void SignInDelegate(bool state, string message);
        public event SignInDelegate OnSignIn;
        public List<SignInDelegate> SignInRequests = new List<SignInDelegate>();

        public delegate void SignOutDelegate(bool state, string message);
        public event SignOutDelegate OnSignOut;
        public List<SignOutDelegate> SignOutRequests = new List<SignOutDelegate>();

        public delegate void ResendConfirmationLinkDelegate(bool state, string message);
        public event ResendConfirmationLinkDelegate OnResendConfirmationLink;
        public List<ResendConfirmationLinkDelegate> ResendConfirmationLinkRequests = new List<ResendConfirmationLinkDelegate>();

        public delegate void AttributeChangedDelegate(bool state, string message);
        public event AttributeChangedDelegate OnAttributeChanged;
        public List<AttributeChangedDelegate> AttributeChangeRequests = new List<AttributeChangedDelegate>();

        public delegate void AttributeConfirmDelegate(bool state, string message);
        public event AttributeConfirmDelegate OnAttributeConfirmed;
        public List<AttributeConfirmDelegate> AttributeConfirmRequests = new List<AttributeConfirmDelegate>();

        public delegate void PasswordChangeDelegate(bool state, string message);
        public event PasswordChangeDelegate OnPasswordChange;
        public List<PasswordChangeDelegate> PasswordChangeRequests = new List<PasswordChangeDelegate>();

        public delegate void RequestNewPasswordDelegate(bool state, string message);
        public event RequestNewPasswordDelegate OnNewPasswordRequest;
        public List<RequestNewPasswordDelegate> NewPasswordRequests = new List<RequestNewPasswordDelegate>();

        public delegate void PasswordResetDelegate(bool state, string message);
        public event PasswordResetDelegate OnPasswordReset;
        public List<PasswordResetDelegate> PasswordResetRequests = new List<PasswordResetDelegate>();

        public delegate void DeleteUserAccountDelegate(bool state, string message);
        public event DeleteUserAccountDelegate OnDeleteUserAccount;
        public List<DeleteUserAccountDelegate> DeleteUserAccountRequests = new List<DeleteUserAccountDelegate>();
               
        public UserActionCallbacks() : base("com.wrlds.api.listeners.UserActionListener")
        {
            WRLDS.userProvider.Call("setUserActionListener", this);
        }
        void onUserAction(string action, bool state, string message)
        {
            switch (action) {
                case "signUp":
                    if (OnSignUp != null)
                        OnSignUp.Invoke(state, message);

                    if (SignUpRequests.Count > 0)
                    {
                        OnSignUp -= SignUpRequests[0];
                        SignUpRequests.RemoveAt(0);
                    }
                    break;
                case "signIn":
                    if (OnSignIn != null)
                        OnSignIn.Invoke(state, message);

                    if (SignInRequests.Count > 0)
                    {
                        OnSignIn -= SignInRequests[0];
                        SignInRequests.RemoveAt(0);
                    }
                    break;
                case "signOut":
                    if (OnSignOut != null)
                        OnSignOut.Invoke(state, message);

                    if (SignOutRequests.Count > 0)
                    {
                        OnSignOut -= SignOutRequests[0];
                        SignOutRequests.RemoveAt(0);
                    }
                    break;
                case "resendConfirmationLink":
                    if (OnResendConfirmationLink != null)
                        OnResendConfirmationLink.Invoke(state, message);

                    if (ResendConfirmationLinkRequests.Count > 0)
                    {
                        OnResendConfirmationLink -= ResendConfirmationLinkRequests[0];
                        ResendConfirmationLinkRequests.RemoveAt(0);
                    }
                    break;
                case "changeAttributes":
                    if (OnAttributeChanged != null)
                        OnAttributeChanged.Invoke(state, message);

                    if (AttributeChangeRequests.Count > 0)
                    {
                        OnAttributeChanged -= AttributeChangeRequests[0];
                        AttributeChangeRequests.RemoveAt(0);
                    }
                    break;
                case "confirmAttribute":
                    if (OnAttributeConfirmed != null)
                        OnAttributeConfirmed.Invoke(state, message);

                    if (AttributeConfirmRequests.Count > 0)
                    {
                        OnAttributeConfirmed -= AttributeConfirmRequests[0];
                        AttributeConfirmRequests.RemoveAt(0);
                    }
                    break;
                case "changePassword":
                    if (OnPasswordChange != null)
                        OnPasswordChange.Invoke(state, message);

                    if (PasswordChangeRequests.Count > 0)
                    {
                        OnPasswordChange -= PasswordChangeRequests[0];
                        PasswordChangeRequests.RemoveAt(0);
                    }
                    break;
                case "requestNewPassword":
                    if (OnNewPasswordRequest != null)
                        OnNewPasswordRequest.Invoke(state, message);

                    if (PasswordChangeRequests.Count > 0)
                    {
                        OnNewPasswordRequest -= NewPasswordRequests[0];
                        NewPasswordRequests.RemoveAt(0);
                    }
                    break;
                case "resetPassword":
                    if (OnPasswordReset != null)
                        OnPasswordReset.Invoke(state, message);

                    if (PasswordResetRequests.Count > 0)
                    {
                        OnPasswordReset -= PasswordResetRequests[0];
                        PasswordResetRequests.RemoveAt(0);
                    }
                    break;
                case "deleteUserAccount":
                    if (OnDeleteUserAccount != null)
                        OnDeleteUserAccount.Invoke(state, message);

                    if (DeleteUserAccountRequests.Count > 0)
                    {
                        OnDeleteUserAccount -= DeleteUserAccountRequests[0];
                        DeleteUserAccountRequests.RemoveAt(0);
                    }
                    break;
            }
        }
    }

#endif
}
